const functions = require('firebase-functions');
const admin = require('firebase-admin');
const sgMail  = require('@sendgrid/mail');
const nodemailer  = require('nodemailer');

const SENDGRID_API_KEY = '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';

const AWS_FROM = '"Confieval" <notificacionesconfieval@gmail.com>';
const AWS_HOST = 'email-smtp.us-east-2.amazonaws.com';
const AWS_USER = 'AKIA4XWCER3RS5NIHG6K';
const AWS_PASSWORD = 'BE5cZkdzuEbDk2mJvqNhPEfSQZjtgCUu4B+53p2DcoEV';

sgMail.setApiKey(SENDGRID_API_KEY);

admin.initializeApp(functions.config().firebase);
//admin.initializeApp();

exports.sendEmailNewClient = functions.database.ref('newCandidate/{id}').onCreate(event => {
  const root =event.ref.root;
  return root.child('newCandidate').once('value').then(async (snapshot) => {
    let snap = snapshot.toJSON();
    let key = Object.keys(snap)[0];
    let mailer = JSON.parse(JSON.stringify(snap[key]));
    const msg = {
        to: mailer.candidateUser,
        from:'noreply@confieval.com',
        cc:'workhonesty2019@gmail.com',
        subject: 'Instrucciones para el candidato',
        templateId:'d-54dcfd140dcb4b0f9c9e209123bcb563',
        substitutionWrappers:['{{','}}'],
        dynamicTemplateData:{
          candidateUser:mailer.candidateUser,
          candidatePassword:mailer.candidatePassword,
          companyName: mailer.companyName,
          companyId: mailer.companyId,
        }
    };
    console.log('Sending: ', JSON.stringify(msg));
    
    let transporter = nodemailer.createTransport({
      host: AWS_HOST,
      port: 587,
      secure: false,
      auth: {
        user: AWS_USER,
        pass: AWS_PASSWORD
      },
    });

    let html = '<html><body><td style="font-size:6px; line-height:10px; padding:0px 0px 0px 0px;" valign="top" align="center"><a href="https://confieval.com"><img class="max-width" border="0" style="display:block; color:#000000; text-decoration:none; font-family:Helvetica, arial, sans-serif; font-size:16px;" src="https://firebasestorage.googleapis.com/v0/b/sistema-ssat.appspot.com/o/CONFIEVAL.png?alt=media&token=5e36e45c-0042-476c-9f54-16e49a66fd47" alt="Confieval" width="600" height="" data-proportionally-constrained="false" data-responsive="false" /></a></td><div><p style="caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-family: Verdana, Geneva, sans-serif; font-size: 13.3333px; font-style: normal; font-variant-caps: normal; font-weight: normal; text-size-adjust: auto;"><span style="font-family:tahoma,geneva,sans-serif;">Estimado&nbsp;Evaluado<br><br>Reciba un cordial saludo, como parte del proceso de selección de {{companyName}}, es necesario que responda la siguiente evaluación. Para poderla llevar a cabo te pedimos que sigas las siguientes instrucciones:<br><br>usuario&nbsp;:&nbsp;<strong>{{candidateUser}}</strong><br>contraseña :&nbsp;</span><span style="font-weight: 600; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-family: tahoma, geneva, sans-serif; font-size: 13.3333px; color: rgb(0, 0, 0); caret-color: rgb(0, 0, 0);">{{candidatePassword}}</span><br><br><br><span style="font-family:tahoma,geneva,sans-serif;"><strong>APLICACIÓN DE LA EVALUACIÓN</strong><br><br><strong>Para poder rendir la evaluación, siga las siguientes instrucciones:&nbsp;</strong></span></p><ul style="caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-family: Verdana, Geneva, sans-serif; font-size: 13.3333px; font-style: normal; font-variant-caps: normal; font-weight: normal; text-size-adjust: auto;"><li><span style="font-family:tahoma,geneva,sans-serif;">Vaya a la página: <a clicktracking="off" href="https://confieval.com/prueba/{{companyId}}" target="_blank">https://confieval.com/prueba/{{companyId}}</a></span></li><li><span style="font-family:tahoma,geneva,sans-serif;">Siga las instrucciones que se muestran en la pantalla.</span></li></ul><p style="caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-family: Verdana, Geneva, sans-serif; font-size: 13.3333px; font-style: normal; font-variant-caps: normal; font-weight: normal; text-size-adjust: auto;"><span style="font-family:tahoma,geneva,sans-serif;">Si tiene algún problema durante el uso del sistema, puede&nbsp;contactarse con nuestro equipo de soporte Técnico al correo: <strong><a clicktracking="off" href="mailto:ayuda@confieval.com">ayuda@confieval.com</a></strong>.<br><br><strong>NOTA:</strong>&nbsp;Confieval®, no solicita información de los evaluados, son las empresas contratantes del servicio, quienes solicitan dicha información y determinan el uso que le dan a la misma.<br>Tampoco es responsable de los resultados de las evaluaciones ni del uso o las decisiones que las organizaciones que la usan hagan de ella.&nbsp;<br>Si tiene cualquier duda o necesita más información, por favor solicítela directamente a la empresa que le asignó la evaluación. Si no desea revelar su información, o llevar a cabo la evaluación, ignore las instrucciones arriba mencionadas y deseche este correo.</span></p></div></body></html>';
    html = html.replace('{{candidateUser}}', msg.dynamicTemplateData.candidateUser);
    html = html.replace('{{candidatePassword}}', msg.dynamicTemplateData.candidatePassword);
    html = html.replace('{{companyName}}', msg.dynamicTemplateData.companyName);
    html = html.replace('{{companyId}}', msg.dynamicTemplateData.companyId);
    html = html.replace('{{companyId}}', msg.dynamicTemplateData.companyId);


    admin.database().ref('newCandidate').remove();

    let info = await transporter.sendMail({
      from: AWS_FROM, 
      to: msg.to,
      subject: msg.subject,
      html
    });

    console.log("Message sent: %s", info.messageId);
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    
    return info;
    // return sgMail.send(msg);
  })
  .then((val)=>console.log('Se ha enviado',val))
  .catch(error=>{
      console.log('error: ', JSON.stringify(error));
  })
});
exports.sendEmailNewClientLink = functions.database.ref('newCandidateLink/{id}').onCreate(event => {
  const root =event.ref.root;
  return root.child('newCandidateLink').once('value').then(async (snapshot) => {
    let snap = snapshot.toJSON();
    let key = Object.keys(snap)[0];
    let mailer = JSON.parse(JSON.stringify(snap[key]));
    const msg = {
        to: mailer.candidateUser,
        from:'noreply@confieval.com',
        cc:'workhonesty2019@gmail.com',
        subject: 'Instrucciones para el candidato',
        templateId:'d-54dcfd140dcb4b0f9c9e209123bcb563',
        substitutionWrappers:['{{','}}'],
        dynamicTemplateData:{
          candidateUser:mailer.candidateUser,
          candidatePassword:mailer.candidatePassword,
          companyName: mailer.companyName,
          companyId: mailer.companyId,
        }
    };
    console.log('Sending: ', JSON.stringify(msg));
    
    let transporter = nodemailer.createTransport({
      host: AWS_HOST,
      port: 587,
      secure: false,
      auth: {
        user: AWS_USER,
        pass: AWS_PASSWORD
      },
    });

    let html = '<html><body><td style="font-size:6px; line-height:10px; padding:0px 0px 0px 0px;" valign="top" align="center"><a href="https://confieval.com"><img class="max-width" border="0" style="display:block; color:#000000; text-decoration:none; font-family:Helvetica, arial, sans-serif; font-size:16px;" src="https://confieval.com/static/media/logo.7fcefe8a.png" alt="Confieval" width="600" height="" data-proportionally-constrained="false" data-responsive="false"></a></td><div><p style="caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-family: Verdana, Geneva, sans-serif; font-size: 13.3333px; font-style: normal; font-variant-caps: normal; font-weight: normal; text-size-adjust: auto;"><span style="font-family:tahoma,geneva,sans-serif;">Estimado&nbsp;Evaluado<br><br>Reciba un cordial saludo, como parte del proceso de selección de {{companyName}}, es necesario que responda la siguiente evaluación. Para poderla llevar a cabo te pedimos que sigas las siguientes instrucciones:<br><br><span style="font-family:tahoma,geneva,sans-serif;"><strong>APLICACIÓN DE LA EVALUACIÓN</strong><br><br><strong>Para poder rendir la evaluación, siga las siguientes instrucciones:&nbsp;</strong></span></p><ul style="caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-family: Verdana, Geneva, sans-serif; font-size: 13.3333px; font-style: normal; font-variant-caps: normal; font-weight: normal; text-size-adjust: auto;"><li><span style="font-family:tahoma,geneva,sans-serif;">Vaya a la página: <a clicktracking="off" href="https://confieval.com/prueba/{{companyId}}/{{candidateUser}}/{{candidatePassword}}" target="_blank">https://confieval.com/prueba/{{companyId}}/{{candidateUser}}/{{candidatePassword}}</a></span></li><li><span style="font-family:tahoma,geneva,sans-serif;">Siga las instrucciones que se muestran en la pantalla.</span></li></ul><p style="caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-family: Verdana, Geneva, sans-serif; font-size: 13.3333px; font-style: normal; font-variant-caps: normal; font-weight: normal; text-size-adjust: auto;"><span style="font-family:tahoma,geneva,sans-serif;">Si tiene algún problema durante el uso del sistema, puede&nbsp;contactarse con nuestro equipo de soporte Técnico al correo: <strong><a clicktracking="off" href="mailto:ayuda@confieval.com">ayuda@confieval.com</a></strong>.<br><br><strong>NOTA:</strong>&nbsp;confieval®, no solicita información de los evaluados, son las empresas contratantes del servicio, quienes solicitan dicha información y determinan el uso que le dan a la misma.<br>Tampoco es responsable de los resultados de las evaluaciones ni del uso o las decisiones que las organizaciones que la usan hagan de ella.&nbsp;<br>Si tiene cualquier duda o necesita más información, por favor solicítela directamente a la empresa que le asignó la evaluación. Si no desea revelar su información, o llevar a cabo la evaluación, ignore las instrucciones arriba mencionadas y deseche este correo.</span></p></div></body></html>';
    html = html.replace('{{candidateUser}}', msg.dynamicTemplateData.candidateUser);
    html = html.replace('{{candidateUser}}', msg.dynamicTemplateData.candidateUser);
    html = html.replace('{{candidatePassword}}', msg.dynamicTemplateData.candidatePassword);
    html = html.replace('{{candidatePassword}}', msg.dynamicTemplateData.candidatePassword);
    html = html.replace('{{companyName}}', msg.dynamicTemplateData.companyName);
    html = html.replace('{{companyName}}', msg.dynamicTemplateData.companyName);
    html = html.replace('{{companyId}}', msg.dynamicTemplateData.companyId);
    html = html.replace('{{companyId}}', msg.dynamicTemplateData.companyId);


    admin.database().ref('newCandidateLink').remove();

    let info = await transporter.sendMail({
      from: AWS_FROM, 
      to: msg.to,
      subject: msg.subject,
      html
    });

    console.log("Message sent: %s", info.messageId);
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    
    return info;
    // return sgMail.send(msg);
  })
  .then((val)=>console.log('Se ha enviado',val))
  .catch(error=>{
      console.log('error: ', JSON.stringify(error));
  })
});

exports.sendEmailNewCompany=functions.database.ref('newCompany/{id}').onCreate(event => {
  console.log('event: ', event);
  const root =event.ref.root;
  console.log('root: ', root);
  return root.child('newCompany').once('value').then(async(snapshot) => {
    console.log('snapshot: ', snapshot);
      let snap = snapshot.toJSON();
      let key = Object.keys(snap)[0];
      let mailer = JSON.parse(JSON.stringify(snap[key]));
      console.log('mailer: ', mailer);
      const msg = {
          to: mailer.mail,
          from:'noreply@confieval.com',
          cc:'workhonesty2019@gmail.com',
          subject: 'Nueva empresa',
          templateId:'d-d75f2fd4046646ed9b1ff6a8c477283e',
          substitutionWrappers:['{{','}}'],
          dynamicTemplateData:{
            mail:mailer.mail,
            name: mailer.name,
            lastname: mailer.lastname,
            password:mailer.password,
            companyName: mailer.companyName,
          }
      };
      console.log('Sending: ', msg);

      let transporter = nodemailer.createTransport({
        host: AWS_HOST,
        port: 587,
        secure: false,
        auth: {
          user: AWS_USER,
          pass: AWS_PASSWORD
        },
      });

      let html = '<html><body><td style="font-size:6px; line-height:10px; padding:0px 0px 0px 0px;" valign="top" align="center"><a href="https://confieval.com"><img class="max-width" border="0" style="display:block; color:#000000; text-decoration:none; font-family:Helvetica, arial, sans-serif; font-size:16px;" src="https://firebasestorage.googleapis.com/v0/b/sistema-ssat.appspot.com/o/CONFIEVAL.png?alt=media&token=5e36e45c-0042-476c-9f54-16e49a66fd47" alt="Confieval" width="600" height="" data-proportionally-constrained="false" data-responsive="false"></a></td><div>Hola {{name}},</div><div>&nbsp;</div><div>El nuevo usuario para la empresa {{companyName}} para usar la plataforma de Confieval es:</div><div>&nbsp;</div><div>usuario: {{mail}}</div><div>contraseña: {{password}}</div><div>&nbsp;</div><div>Puedes ingresar a la plataforma desde :&nbsp;</div><div><a clicktracking="off" href="https://confieval.com" target="_blank">https://confieval.com</a></div><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div></body></html>';
      html = html.replace('{{mail}}',msg.dynamicTemplateData.mail);
      html = html.replace('{{name}}',msg.dynamicTemplateData.name);
      html = html.replace('{{lastname}}',msg.dynamicTemplateData.lastname);
      html = html.replace('{{password}}',msg.dynamicTemplateData.password);
      html = html.replace('{{companyName}}',msg.dynamicTemplateData.companyName);
      // return sgMail.send(msg);
      admin.database().ref('newCompany').remove();

      let info = await transporter.sendMail({
        from: AWS_FROM, 
        to: msg.to,
        subject: msg.subject,
        html
      });
  
      console.log("Message sent: %s", info.messageId);
      console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
      
      return info;
  })
  .then((val)=>console.log('Se ha enviado',val))
  .catch(error=>{
      console.log('error: ', error);
  })
});
exports.sendEmailNewUser=functions.database.ref('newUser/{id}').onCreate(event => {
  const root =event.ref.root;
  return root.child('newUser').once('value').then(async(snapshot) => {
      let snap = snapshot.toJSON();
      let key = Object.keys(snap)[0];
      let mailer = snap[key];
      console.log('mailer: ', mailer);
      const msg = {
          to: mailer.mail,
          from:'noreply@confieval.com',
          cc:'workhonesty2019@gmail.com',
          subject: 'Nuevo usuario',
          templateId:'d-d75f2fd4046646ed9b1ff6a8c477283e',
          substitutionWrappers:['{{','}}'],
          dynamicTemplateData:{
            mail:mailer.mail,
            name: mailer.name,
            lastname: mailer.lastname,
            password:mailer.password,
            companyName: mailer.companyName,
          }
      };
      console.log('Sending: ', msg);
      admin.database().ref('newUser').remove();
      console.log('Sending: ', msg);

      let transporter = nodemailer.createTransport({
        host: AWS_HOST,
        port: 587,
        secure: false,
        auth: {
          user: AWS_USER,
          pass: AWS_PASSWORD
        },
      });

      let html = '<html><body><td style="font-size:6px; line-height:10px; padding:0px 0px 0px 0px;" valign="top" align="center"><a href="https://confieval.com"><img class="max-width" border="0" style="display:block; color:#000000; text-decoration:none; font-family:Helvetica, arial, sans-serif; font-size:16px;" src="https://firebasestorage.googleapis.com/v0/b/sistema-ssat.appspot.com/o/CONFIEVAL.png?alt=media&token=5e36e45c-0042-476c-9f54-16e49a66fd47" alt="Confieval" width="600" height="" data-proportionally-constrained="false" data-responsive="false"></a></td><div>Hola {{name}},</div><div>&nbsp;</div><div>El nuevo usuario para la empresa {{companyName}} para usar la plataforma de Confieval es:</div><div>&nbsp;</div><div>usuario: {{mail}}</div><div>contraseña: {{password}}</div><div>&nbsp;</div><div>Puedes ingresar a la plataforma desde :&nbsp;</div><div><a clicktracking="off" href="https://confieval.com" target="_blank">https://confieval.com</a></div><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div></body></html>';
      html = html.replace('{{mail}}',msg.dynamicTemplateData.mail);
      html = html.replace('{{name}}',msg.dynamicTemplateData.name);
      html = html.replace('{{lastname}}',msg.dynamicTemplateData.lastname);
      html = html.replace('{{password}}',msg.dynamicTemplateData.password);
      html = html.replace('{{companyName}}',msg.dynamicTemplateData.companyName);
      // return sgMail.send(msg);
      admin.database().ref('newCompany').remove();

      let info = await transporter.sendMail({
        from: AWS_FROM,
        to: msg.to,
        subject: msg.subject,
        html
      });
  
      console.log("Message sent: %s", info.messageId);
      console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
      
      return info;
  })
  .then((val)=>console.log('Se ha enviado',val))
  .catch(error=>{
      console.log('error: ', error);
  })
});



/*
// Configure the email transport using the default SMTP transport and a GMail account.
// For Gmail, enable these:
// 1. https://www.google.com/settings/security/lesssecureapps
// 2. https://accounts.google.com/DisplayUnlockCaptcha
// For other types of transports such as Sendgrid see https://nodemailer.com/transports/
// TODO: Configure the `gmail.email` and `gmail.password` Google Cloud environment variables.
const gmailEmail = functions.config().gmail.email;
const gmailPassword = functions.config().gmail.password;
const mailTransport = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: gmailEmail,
    pass: gmailPassword,
  },
});

// Your company name to include in the emails
// TODO: Change this to your app or company name to customize the email sent.
const APP_NAME = 'Cloud Storage for Firebase quickstart';

// [START sendWelcomeEmail]
 //Sends a welcome email to new user.
// [START onCreateTrigger]
exports.sendWelcomeEmail = functions.auth.user().onCreate((user) => {
// [END onCreateTrigger]
  // [START eventAttributes]
  const email = user.email; // The email of the user.
  const displayName = user.displayName; // The display name of the user.
  // [END eventAttributes]

  return sendWelcomeEmail(email, displayName);
});
// [END sendWelcomeEmail]

// [START sendByeEmail]

// [START onDeleteTrigger]
exports.sendByeEmail = functions.auth.user().onDelete((user) => {
// [END onDeleteTrigger]
  const email = user.email;
  const displayName = user.displayName;

  return sendGoodbyEmail(email, displayName);
});
// [END sendByeEmail]

// Sends a welcome email to the given user.
function sendWelcomeEmail(email, displayName) {
  const mailOptions = {
    from: `${APP_NAME} <noreply@firebase.com>`,
    to: email,
  };

  // The user subscribed to the newsletter.
  mailOptions.subject = `Welcome to ${APP_NAME}!`;
  mailOptions.text = `Hey ${displayName || ''}! Welcome to ${APP_NAME}. I hope you will enjoy our service.`;
  return mailTransport.sendMail(mailOptions).then(() => {
    return console.log('New welcome email sent to:', email);
  });
}

// Sends a goodbye email to the given user.
function sendGoodbyEmail(email, displayName) {
  const mailOptions = {
    from: `${APP_NAME} <noreply@firebase.com>`,
    to: email,
  };

  // The user unsubscribed to the newsletter.
  mailOptions.subject = `Bye!`;
  mailOptions.text = `Hey ${displayName || ''}!, We confirm that we have deleted your ${APP_NAME} account.`;
  return mailTransport.sendMail(mailOptions).then(() => {
    return console.log('Account deletion confirmation email sent to:', email);
  });
}
*/